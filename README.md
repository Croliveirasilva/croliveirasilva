### Oi  👋

Eu sou Carlos Rafael sou especialista em conteúdo atualmente estou desenvolvendo em  PHP - C# - AngularJs, alem disso estudo React - React-Native

<!---**Croliveirasilva/croliveirasilva** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile. -->

Aqui estão algumas ideias para você começar:

🔭Atualmente estou trabalhando como desenvolvedor de Software na BELFAR Industria farmacêutica.

📱Tenho uma empresa onde fazemos trabalhos de marketing e desenvolvemos jogos e aplicativos

👨‍💻Todos os meus projetos estão disponíveis em trabalhos.carlosrafael.com.br

▶ ️Eu regularmente posto vídeos em https://www.youtube.com/c/CROSContent

📫Como entrar em contato comigo contato@carlosrafael.com.br

⚡Curiosidade 😜
<p align="left"><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/inkscape/inkscape-original-wordmark.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/inkscape/inkscape-original-wordmark.svg" alt="inkscape" width="20" height="20" style="max-width:100%;"></a><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/master/icons/figma/figma-original.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/figma/figma-original.svg" alt="figma" width="20" height="20" style="max-width:100%;"></a><a target="_blank" rel="noopener noreferrer" href="hthttps://raw.githubusercontent.com/devicons/devicon/master/icons/photoshop/photoshop-line.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/photoshop/photoshop-line.svg" alt="photoshop" width="20" height="20" style="max-width:100%;"></a><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/illustrator/illustrator-line.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/illustrator/illustrator-line.svg" alt="illustrator" width="20" height="20" style="max-width:100%;"></a>

<a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="20" height="20" style="max-width:100%;"></a><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-plain-wordmark.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-plain-wordmark.svg" alt="css3" width="20" height="20" style="max-width:100%;"></a><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/master/icons/angularjs/angularjs-original.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/angularjs/angularjs-original.svg" alt="angularjs" width="20" height="20" style="max-width:100%;"></a><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/react/react-original-wordmark.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/react/react-original-wordmark.svg" alt="react" width="20" height="20" style="max-width:100%;"></a>

<a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/csharp/csharp-original.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/csharp/csharp-original.svg" alt="csharp" width="20" height="20" style="max-width:100%;"></a><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/php/php-original.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/php/php-original.svg" alt="php" width="20" height="20" style="max-width:100%;"></a><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/typescript/typescript-original.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/typescript/typescript-original.svg" alt="typescript" width="20" height="20" style="max-width:100%;"></a><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="20" height="20" style="max-width:100%;"></a>


<a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/master/icons/microsoftsqlserver/microsoftsqlserver-plain-wordmark.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/microsoftsqlserver/microsoftsqlserver-plain-wordmark.svg" alt="microsoftsqlserver" width="20" height="20" style="max-width:100%;"></a><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="20" height="20" style="max-width:100%;"></a>

<a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/laravel/laravel-plain-wordmark.svg"><img src="https://raw.githubusercontent.com/devicons/devicon/c7d326b6009e60442abc35fa45706d6f30ee4c8e/icons/laravel/laravel-plain-wordmark.svg" alt="laravel" width="20" height="20" style="max-width:100%;"></a>

</p>


